# SPDX-FileCopyrightText: 2023-present Alexandre Marty <alexandre@marty.in>
#
# SPDX-License-Identifier: MIT
__version__ = "1.1.0"
